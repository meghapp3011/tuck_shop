from django import forms
from .models import Student,Inventory,Purchase

class StudentForm(forms.ModelForm):
    class Meta:
        model = Student
        fields = ['name', 'grade', 'mail', 'limit']

class PurchaseForm(forms.ModelForm):
    purchase_date = forms.DateField(widget=forms.DateInput(attrs={'type': 'date'}))
    class Meta:
        model = Purchase
        fields = ['student_name', 'item','purchase_date','purchased_quantity']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # Query the database to get the choices for the dropdowns
        self.fields['student_name'].queryset = Student.objects.all()
        self.fields['item'].queryset = Inventory.objects.all()

class InventoryForm(forms.ModelForm):
    last_restock = forms.DateField(widget=forms.DateInput(attrs={'type': 'date'}))

    class Meta:
        model = Inventory
        fields = ['item_name', 'last_restock', 'quantity', 'price', ]
        widgets = {
            'availabity': forms.CheckboxInput(),
        }