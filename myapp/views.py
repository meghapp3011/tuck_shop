from django.forms import modelformset_factory
from django.shortcuts import render,redirect
from django.contrib.auth import authenticate,login
from django.contrib.auth.decorators import login_required
from .forms import StudentForm
from .models import Student,Purchase,Inventory
from .forms import PurchaseForm,InventoryForm ,StudentForm 
from django.db.models import Q
from django.db.models import Sum
from datetime import datetime
from collections import Counter
import pandas as pd
from django.http import HttpResponse
import io 
from xlsxwriter.workbook import Workbook
from django.core.exceptions import ObjectDoesNotExist



def download_excel_sheet(request):
    month = request.GET.get('month')
    year = request.GET.get('year')
    if month and year:
        purchases = Purchase.objects.filter(purchase_date__year=year, purchase_date__month=month)
    else:
        purchases = Purchase.objects.all()
    
    # Aggregate the data by student_name and calculate the total amount for each student
    student_data = purchases.values('student_name__name', 'student_name__id', 'student_name__mail').annotate(total_amount=Sum('total_price'))
    
    # Create a DataFrame from the aggregated data
    df = pd.DataFrame(student_data)
    
    # Prepare the buffer to hold the Excel file
    buffer = io.BytesIO()
    writer = pd.ExcelWriter(buffer, engine='xlsxwriter')
    
    # Write the DataFrame to the Excel file
    df.to_excel(writer, index=False)
    writer.close()
    buffer.seek(0)
    
    # Set the appropriate response headers for downloading the Excel file
    response = HttpResponse(buffer, content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    response['Content-Disposition'] = 'attachment; filename=student_data.xlsx'
    
    return response

# def add_purchase(request):
#     inventory_items = Inventory.objects.all()
#     purchases = Purchase.objects.all()
#     total_price = 0
#     error_message = ""

#     if request.method == 'POST':
#         form = PurchaseForm(request.POST)
#         if form.is_valid():
#             student_name = form.cleaned_data['student_name']
#             item = form.cleaned_data['item']
#             purchased_quantity = form.cleaned_data['purchased_quantity']
#             item_price = item.price  # Use the item price from the selected item
#             purchase_date = form.cleaned_data['purchase_date']
#             try:
#                 student = Student.objects.get(name=student_name)
#                 limit = student.limit
#             except Student.DoesNotExist:
#                 limit_rate = 0  # Set a default value for the limit rate if the student is not found

#             if item.availability and item.quantity >= purchased_quantity and purchased_quantity > 0:
#                 total_price = item_price * purchased_quantity

#                 item.quantity -= purchased_quantity
#                 item.save()
#                 if item_price > limit:
#                     error_message = "The selected item's price exceeds the limit rate. You cannot purchase this item."

#                 # Save carted item to Purchase table
#                 purchase = Purchase(
#                     student_name=student_name,
#                     item=item,
#                     purchased_quantity=purchased_quantity,
#                     total_price=total_price,
#                     purchase_date=purchase_date
#                 )
#                 purchase.save()

#                 # Clear the form for adding the next item
#                 form = PurchaseForm()

#             else:
#                 if not item.availability:
#                     error_message = "The selected item is not available in the inventory."
#                 elif item.quantity < purchased_quantity or purchased_quantity <= 0:
#                     error_message = "The quantity is invalid or exceeds the available quantity."

#         else:
#             error_message = "The form is not valid. Please check the input fields."

#     else:
#         form = PurchaseForm()

#     return render(request, 'add_purchase.html', {'inventory_items': inventory_items, 'purchases': purchases, 'form': form, 'total_price': total_price, 'error_message': error_message})


def add_to_cart(request):
    inventory_items = Inventory.objects.all()
    purchases = Purchase.objects.all()
    total_price = 0
    error_message = ""

    if request.method == 'POST':
        form = PurchaseForm(request.POST)
        if form.is_valid():
            student_name = form.cleaned_data['student_name']
            item = form.cleaned_data['item']
            purchased_quantity = form.cleaned_data['purchased_quantity']
            item_price = item.price  # Use the item price from the selected item
            purchase_date = form.cleaned_data['purchase_date']
            try:
                student = Student.objects.get(name=student_name)
                limit = student.limit
            except Student.DoesNotExist:
                limit = 0  # Set a default value for the limit rate if the student is not found

            if item.availability and item.quantity >= purchased_quantity and purchased_quantity > 0:
                total_price = item_price * purchased_quantity

                item.quantity -= purchased_quantity
                item.save()

            if item_price > limit:
                error_message = "The selected item's price exceeds the limit rate. You cannot purchase this item."

                # Save carted item to Purchase table
                purchase = Purchase(
                    student_name=student_name,
                    item=item,
                    purchased_quantity=purchased_quantity,
                    total_price=total_price,
                    purchase_date=purchase_date
                )
                purchase.save()

                # Clear the form for adding the next item
                form = PurchaseForm()

            else:
                if not item.availability:
                    error_message = "The selected item is not available in the inventory."
                elif item.quantity < purchased_quantity or purchased_quantity <= 0:
                    error_message = "The quantity is invalid or exceeds the available quantity."

        else:
            error_message = "The form is not valid. Please check the input fields."

    else:
        form = PurchaseForm()

    return render(request, 'add_to_cart.html', {'inventory_items': inventory_items, 'purchases': purchases, 'form': form, 'total_price': total_price, 'error_message': error_message})

# def view_cart(request):
#     cart_items = Purchase.objects.all()
#     total_cart_price = sum(item.total_price for item in cart_items)
#     return render(request, 'view_cart.html', {'cart_items': cart_items, 'total_cart_price': total_cart_price})

def view_cart(request):
    cart = request.session.get('cart', {})
    cart_items = []
    total_cart_price = 0

    for cart_item in cart.values():
        item = Inventory.objects.get(id=cart_item['item_id'])
        total_cart_price += cart_item['total_price']
        cart_items.append({
            'item': item,
            'purchased_quantity': cart_item['purchased_quantity'],
            'total_price': cart_item['total_price'],
            'purchase_date': cart_item['purchase_date'],
        })

    return render(request, 'view_cart.html', {'cart_items': cart_items, 'total_cart_price': total_cart_price})

def buy_items(request):
    cart = request.session.get('cart', {})
    for cart_item in cart.values():
        try:
            student_name = cart_item['student_name']
            item_id = cart_item['item_id']
            purchased_quantity = cart_item['purchased_quantity']
            total_price = cart_item['total_price']
            purchase_date = cart_item['purchase_date']

            item = Inventory.objects.get(id=item_id)
            student = Student.objects.get(name=student_name)
            limit = student.limit

            if item.availability and item.quantity >= purchased_quantity and purchased_quantity > 0:
                # Save carted item to Purchase table
                purchase = Purchase(
                    student_name=student_name,
                    item=item,
                    purchased_quantity=purchased_quantity,
                    total_price=total_price,
                    purchase_date=purchase_date
                )
                purchase.save()

                # Update inventory quantity
                item.quantity -= purchased_quantity
                item.save()

        except (ObjectDoesNotExist, KeyError):
            # Handle the case when the item or student does not exist, or the cart item is missing some keys
            return redirect('purchase')

    # Clear the cart after buying items
    request.session['cart'] = {}

    return redirect('purchase')



@login_required
def home_view(request):
    return render(request, 'home.html')

def login_view(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request,username=username, password=password)
        if user is not None:
            login(request,user)
            return redirect('home')
        else:
            error_message = "invalid username or password"
            return render(request,'login.html',{'error_message':error_message})
    else:
        return render(request,'login.html')

def students(request):
    search_query = request.GET.get('search_query', '')
    students = Student.objects.all()

    if search_query:
        students = Student.objects.filter(name__icontains=search_query)

    context = {
        'students': students,
    }
    return render(request, 'students.html', context)

def add_student(request):
    if request.method == 'POST':
        form = StudentForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('student')
    else:
        form = StudentForm()
    
    return render(request, 'add_student.html', {'form': form})

def purchase(request):
    search_query = request.GET.get('search_query', '')
    purchases = Purchase.objects.all()

    if search_query:
         purchases = Purchase.objects.filter(
            Q(student_name__name__icontains=search_query) |
            Q(item__item_name__icontains=search_query)
        )

    context = {
        'purchases': purchases,
    }
    return render(request, 'purchase.html', context)


def inventory(request):
    search_query = request.GET.get('search_query', '')
    inventory_items = Inventory.objects.all()

    if search_query:
        inventory_items = Inventory.objects.filter(
            Q(item_name__icontains=search_query) |
            Q(last_restock__icontains=search_query)
        )

    context = {
        'inventory_items': inventory_items,
    }
    return render(request, 'inventory.html', context)


def add_inventory(request):
    if request.method == 'POST':
        form = InventoryForm(request.POST)
        if form.is_valid():
            item = form.save(commit=False)
            item.availability = True  # Set the availability to True for each newly added item
            item.save()
            return redirect('inventory')  # Redirect to the inventory page after successfully adding the item
    else:
        form = InventoryForm()

    return render(request, 'add_inventory.html', {'form': form})

def update_inventory(request, id):
    inventory = Inventory.objects.get(id=id)

    if request.method == 'POST':
        form = InventoryForm(request.POST, instance=inventory)
        if form.is_valid():
            item = form.save(commit=False)
            if item.quantity >=1 :
                item.availability = True
            else:
                item.availability = False # Set the availability to True for each newly added item
            item.save()
            form.save()
            return redirect('inventory')
    else:
        form = InventoryForm(instance=inventory)

    context = {
        'form': form,
        'id': id
    }

    return render(request, 'update_inventory.html', context)



def update_student(request, id):
    student = Student.objects.get(id=id)

    if request.method == 'POST':
        form = StudentForm(request.POST, instance=student)
        if form.is_valid():
            form.save()
            return redirect('student')
    else:
        form = StudentForm(instance=student)

    context = {
        'form': form,
        'id': id
    }

    return render(request, 'update_student.html', context)


def delete_student(request, id):
    try:
        student = Student.objects.get(id=id)
        student.delete()
    except Student.DoesNotExist:
        pass

    return redirect('student')

def delete_purchase(request, id):
    try:
        purchase = Purchase.objects.get(id=id)
        purchase.delete()
    except Purchase.DoesNotExist:
        pass

    return redirect('purchase')

def delete_inventory(request,id):
    try:
        inventory = Inventory.objects.get(id=id)
        inventory.delete()
    except Inventory.DoesNotExist:
        pass

    return redirect('inventory')


def graph_view(request):
    # Retrieve all purchased items from the Purchase table
    purchased_items = Purchase.objects.all()

    # Use Counter to group and count the quantities for each item
    item_quantities = Counter()

    for item in purchased_items:
        item_quantities[item.item.item_name] += item.purchased_quantity

    # Extract data for the chart
    item_names = list(item_quantities.keys())
    quantities = list(item_quantities.values())

    context = {
        'item_names': item_names,
        'quantities': quantities,
    }

    return render(request, 'graph.html', context)

def student_graph_view(request):
    month = request.GET.get('month')
    year = request.GET.get('year')
    selected_student_id = request.GET.get('student')  # Get the selected student ID from the form

    # Convert the month and year into a datetime object
    if month and year:
        date_string = f"{year}-{month}-01"
        selected_date = datetime.strptime(date_string, "%Y-%m-%d")

        # Retrieve all purchases for the selected month and year
        purchases = Purchase.objects.filter(purchase_date__year=selected_date.year, purchase_date__month=selected_date.month)

        # Filter purchases based on the selected student, if a student is selected
        if selected_student_id:
            purchases = purchases.filter(student_name_id=selected_student_id)

        # Group purchases by student and item and calculate the total quantity purchased for each item by each student
        student_data = purchases.values('student_name__name', 'item__item_name').annotate(total_quantity=Sum('purchased_quantity'))

        # Extract data for the chart
        student_names = list(set(data['student_name__name'] for data in student_data))
        item_names = list(set(data['item__item_name'] for data in student_data))
        total_quantities = {name: [data['total_quantity'] for data in student_data if data['item__item_name'] == name] for name in item_names}

        # Retrieve the list of all students
        all_students = Student.objects.all()
    else:
        # If month and year are not provided in the URL, set default values
        student_names = []
        item_names = []
        total_quantities = {}
        all_students = []

    context = {
        'student_names': student_names,
        'item_names': item_names,
        'total_quantities': total_quantities,
        'selected_month': month,
        'selected_year': year,
        'all_students': all_students,
        'selected_student_id': selected_student_id,  # Pass the selected student ID to the template
    }

    return render(request, 'student_graph.html', context)
