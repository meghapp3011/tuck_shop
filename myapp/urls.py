"""
URL configuration for Tuck_shop project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from .import views

urlpatterns = [
    path('login/',views.login_view,name="login"),
    path('home/',views.home_view,name="home"),
    path('student/', views.students, name='student'),
    path('add-student/', views.add_student, name='add_student'),
    path('purchase/', views.purchase, name='purchase'),
    # path('purchase/add/', views.add_purchase, name='add_purchase'),
    path('inventory/', views.inventory, name='inventory'),
    path('inventory/add/', views.add_inventory, name='add_inventory'),
    path('inventory/update/<int:id>/', views.update_inventory, name='update_inventory'),
    path('inventory/delete/<int:id>/', views.delete_inventory, name='delete_inventory'),
    path('student/update/<int:id>/', views.update_student, name='update_student'),
    path('student/delete/<int:id>/', views.delete_student, name='delete_student'),
    path('purchase/delete/<int:id>/', views.delete_purchase, name='delete_purchase'),
    # path('save_purchase/', views.save_purchase, name='save_purchase'),
    path('graph/', views.graph_view, name='graph'),
    path('student_graph/', views.student_graph_view, name='student_graph'),
    # path('add_purchase/', views.add_purchase, name='add_purchase'),
    path('download_excel_sheet/', views.download_excel_sheet, name='download_excel_sheet'),
    path('add_to_cart/', views.add_to_cart, name='add_to_cart'),
    path('view_cart/', views.view_cart, name='view_cart'),
    path('buy_item/', views.buy_items, name='buy_item'),



]
