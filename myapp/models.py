from django.db import models

# Models go here


class Student(models.Model):
    name = models.CharField(max_length=100)
    grade = models.CharField(max_length=20)
    mail = models.EmailField()
    limit = models.IntegerField()

    def __str__(self):
        return self.name

 
class Inventory(models.Model):
    item_name = models.CharField(max_length=100)
    last_restock = models.DateField()
    quantity = models.IntegerField()
    price = models.DecimalField(max_digits=10, decimal_places=2)
    availability = models.BooleanField(default=False)

    def __str__(self):
        return self.item_name

    
class Purchase(models.Model):
    student_name = models.ForeignKey(Student, on_delete=models.CASCADE)
    item =  models.ForeignKey(Inventory,on_delete=models.CASCADE,default=False)
    purchase_date = models.DateField()
    total_price = models.IntegerField(null=True)
    purchased_quantity = models.IntegerField(default=False)

    def __str__(self):
        return f"Purchase by {self.student_name}"
